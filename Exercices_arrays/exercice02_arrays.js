// exercice 2 : faire un tableau avec toutes les chambres de tous les hotels,
// et ne garder que les chambres qui
// ont plus que 3 places ou exactement 3 places et
// les classer par ordre alphabétique selon le non de la chambre

//Méthode 1

function exercice2 () {
   
    let rooms = getHostel()
                .map(showRooms2)
                .reduce(fusion)
                .sort(compare2);
    
     return rooms;
}

function showRooms2 (value) {
    
    let room = value.rooms.filter(select2)
    
    return room;
}

function select2 (user) {
    
    return  user.size >= 3;
}


function compare2 (a, b){
    
    var x = a.roomName;
    var y = b.roomName;
    if (x < y) {return -1;}
    else {return 1;}
    return 0;
    
  };

function fusion (a, b) {
    
    return a.concat(b);
}

//Solution


function solution_ex2() {
    
    const flatHostels = getHostel()
    .reduce((acc, hotel) => acc.concat(hotel.rooms), [])
    .filter((room) => room.size >= 3)
    .sort((a, b) => b.roomName < a.roomName ? 1 : -1);
    
    
    return flatHostels;

}