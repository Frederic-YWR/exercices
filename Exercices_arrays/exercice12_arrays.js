// exercice 12 : faire une fonction qui prend en paramètre la liste des hotel,
// un id d'hotel et une taille et qui renvoie le nom et l'id de toutes
// les chambres de cet hotel qui font exactement la taille indiquée


function exercice12(hostel_list,hostel_id,hostel_size) {
    
    let selected_rooms;
    let browse = getHostel()[ getHostel().map(fill_by_index).indexOf(hostel_id)];
    browse == undefined ? selected_rooms = "aucun résultat": selected_rooms  = browse.rooms.filter(room,hostel_size);
 
    
    return selected_rooms;
    
}

function room(value,index) {
     
     return value.size == parseInt(this);
}
function  fill_by_index (value) {
   
    return value.id;
}

//Solution

function findHostelById(hostels, hotelId) {
  const hostelToFind = hostels && hotelId
      ? hostels.find((hotel) => hotel.id === hotelId)
      : null;
  return hostelToFind ? hostelToFind : null;
}
function solution_ex12(hostels, hostelId, size) {
    
  const hostelToFind = findHostelById(hostels, hostelId);
  return hostelToFind.rooms
      .filter((room) => room.size === size)
      .map((room) => ({name: room.roomName, id: room.id}));
}