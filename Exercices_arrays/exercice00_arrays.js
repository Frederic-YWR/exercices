// exercice 0 : mettre une majuscule à toutes les RoomName



//Méthode 1 (impératif)

function exercice0_1() {
   
   let classement = [];
    
   let hostels = getHostel();
    
   for (let i = 0;i < hostels.length ; ++i) {
       
       
       let array_hostel = [];
       
       for(let j = 0; j < hostels[i].roomNumbers;++j) {
           
           let str = hostels[i].rooms[j].roomName;
           let res = str.replace( str[0],str[0].toUpperCase());
           array_hostel.push(res);
       }
       
       classement[hostels[i].name] = array_hostel;
       array_hostel = [];
   }
    
    console.log(classement);
}

//Méthode 2 (fonctionnelle)


function exercice0_2 () {
    
   let hostels = getHostel().map(browse_hotels);
    
 
   return hostels;
}

function browse_hotels (user) {
    
   user.rooms = user.rooms.map(select) ;
    
  return user ;
    
}

function select (user) {
    
     let str = user.roomName
               .replace( user.roomName[0],user.roomName[0].toUpperCase());
     return str;
}

// Solution

function solution_ex0() {
    
    
    function capi(name) {
  return name && typeof name === 'string'
      ? name.charAt(0).toUpperCase() + name.slice(1)
      : '';
}

function capiAllRooms(hostels) {
  return hostels.map((hotel) => {
    hotel.rooms = hotel.rooms.map((room) => {
      room.roomName = capi(room.roomName);
      return room;
    });
    return hotel;
  });
    
}
    
    return capiAllRooms( getHostel());
    
}