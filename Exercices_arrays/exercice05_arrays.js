// exercice 5  : extraire du tableau hostels l'hotel qui a le nom 'hotel ocean' en le supprimant du tableau,
// et le mettre dans une nouvelle variable
// puis effacer toutes ses chambres et mettre à jour sa valeur room number, puis pusher l'hotel modifié dans hostels
// puis faire un sort par nom d'hotel
// puis donner le nouvel index de l'hotel océan (faire 2 méthodes : avec indexOf et avec un foreach)


function exercice5() {
    
let hostels_list = clean().sort(compare_alpha);
let newIndex;
hostels_list.forEach((hotel, index) => {
  if (hotel.name === 'hotel ocean') {
    newIndex = index;
  }
    
})
    
    return newIndex;
}

  function compare_alpha (a, b){
    var x = a.name;
    var y = b.name;
    if (x < y) {return -1;}
    if (x > y) {return 1;}
    return 0;
  };


function clean () {
    
    
   getHostel().forEach(hostel);
    
    return hostels;
}

function hostel (user,index) {
    
    if (user.name === 'hotel ocean') {
        
        let ocean = hostels.splice(index,1);
        ocean[0].rooms = [];
        ocean[0].roomNumbers = 0;
        hostels[hostels.length] = ocean[0];
        
    }
    
}

//Solution



function solution_ex5 () {
    
const indexToExtract = getHostel()
    .findIndex((hotel) => hotel.name === 'hotel ocean');
let hostelsCopy = getHostel();
const [extractedHostel] = hostelsCopy.splice(indexToExtract, 1);
extractedHostel.rooms = [];
hostelsCopy.push(extractedHostel);
hostelsCopy = hostelsCopy.sort((a, b) => b.name < a.name ? 1 : -1);
const newIndex1 = hostelsCopy.indexOf(extractedHostel);
let newIndex2 = null;
hostelsCopy.forEach((hotel, index) => {
  if (hotel.name === 'hotel ocean') {
    newIndex2 = index;
  }
    
});
    return newIndex2;
}


