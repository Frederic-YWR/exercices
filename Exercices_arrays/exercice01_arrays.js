
// exercice 1 : trier les hotels par nombre
// de chambres (plus grand en 1er) et créer un tableau
// contenant seulement
// le nom des hotels dans leur ordre de tri



function exercice1 () {
    
  let hostels = getHostel().sort(compare)
                .map(hostel);
    
  return hostels;
    
}

function compare (a, b){return b.roomNumbers - a.roomNumbers};

function hostel(value) {
    
    return value.name
}



//Solution


function solution_ex1 () {
    
    const sortedHostels = getHostel()
    .sort((a, b) => b.roomNumbers - a.roomNumbers)
    .map((hostel) => hostel.name);

    return sortedHostels;
}