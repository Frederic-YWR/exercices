// exercice 9: faire une fonction qui prend en paramètre la liste des hotels
// et qui vérifie que toutes les chambres des hotels ont bien
// une majuscule a leur nom et qui renvoie un boolean donnant le résultat.
// Puis faire en sorte que la liste des hotels valide bien cette fonction.




function exercice9() {
    
    let hostels =  getHostel();
    
    let result = hostels.map(hostel);
    
    return result;
    
}

function hostel(value) {
    
    let hostel = value.rooms.every(room);
    
    return hostel
}

function room(value) {
     
    return value.roomName[0] === value.roomName[0].toUpperCase()
    
}

function exercice9_bis() {
    
    let hotels = getHostel().map(hostel_bis);
    
    return hotels
}
function hostel_bis(value) {
    
    value.rooms.map(room_bis);
    
    return value
}
function room_bis(value) {
    
      let str = value.roomName;
      let res = str.replace( str[0],str[0].toUpperCase());
      value.roomName = res;
    
    return value
}

//Solution

function testIfRoomNameIsCapi(room) {
  return room.roomName === capi(room.roomName);
}
function capi(name) {
  return name && typeof name === 'string'
      ? name.charAt(0).toUpperCase() + name.slice(1)
      : '';
}

function capiAllRooms(hostels) {
  return getHostel().map((hotel) => {
    hotel.rooms = hotel.rooms.map((room) => {
      room.roomName = capi(room.roomName);
      return room;
    });
    return hotel;
  });
}

  function testAllRoomsCapi(value) {
  return value
      .reduce((acc, hotel) => acc.concat(hotel.rooms), [])
      .every(testIfRoomNameIsCapi);
}


function solution_ex9() {
    
 
    return testAllRoomsCapi(getHostel());
}

function solution_ex9bis() {
 
 
    
  return testAllRoomsCapi(capiAllRooms(getHostel()))
}

