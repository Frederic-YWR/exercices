// exercice 3 : faire un tableau avec toutes les chambres des hotels
// et ne garder que les chambres qui ont plus de 3 places et dont le
// nom de chambre a une taille supérieure à 15 charactères


function exercice3 () {
  
    return  getHostel().map(showRooms).reduce(fusion);
}

function showRooms (value) {
    
    return value.rooms.filter(select2);
}
function select2 (user) {
    
    return user.roomName.length > 15 && user.size > 3 ;
}
function fusion (a, b) {
    
    return a.concat(b);
}

//Solution

function solution_ex3 () {
    
    const flatAndUpper15Hostels = getHostel()
    .reduce((acc, hotel) => acc.concat(hotel.rooms), [])
    .filter((room) => room.size > 3)
    .filter((room) => room.roomName.length > 15);
    
    return flatAndUpper15Hostels;

}