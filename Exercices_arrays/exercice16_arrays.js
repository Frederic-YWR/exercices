// exercice 16 : faire une fonction qui prend en paramètre la liste des hotels,
// une id d'hotel et une nouvelle chambre (avec son nom et sa taille) qui
// ajoute la nouvelle chambre dans l'hote concerné et lui donne un id qui suit le
// dernier id de l'hotel


function exercice16(hostel_list,hostel_id,room_name,areas) {
    
    let indexed_hotels = hostel_list.map(fill_by_index);
    let targeted_hotel = indexed_hotels.indexOf(hostel_id);
    
    let arrayOfIndex = hostel_list[targeted_hotel].rooms.map(rooms);
    
    let clean = arrayOfIndex.reduce(compare);
    
    let obj = {roomname:room_name,size :areas,id : clean + 1};
    
    
     hostels[targeted_hotel].rooms[arrayOfIndex.length] = obj;
    
     hostels[targeted_hotel].roomNumbers =  hostels[targeted_hotel].rooms.length;
    
    return hostels;
}
function  fill_by_index (value) {
   
    return value.id;
}

function rooms(value) {
    
    
    return value.id;
}

function compare(num1, num2) {
    if(num1 < num2) return num2;
}


//Solution

function findHostelById(hostels, hotelId) {
  const hostelToFind = hostels && hotelId
      ? hostels.find((hotel) => hotel.id === hotelId)
      : null;
  return hostelToFind ? hostelToFind : null;
}

function solution_ex16(hostels, hostelId, newRoom) {
  const hostelToFind = findHostelById(hostels, hostelId);

  hostelToFind.rooms.push({...newRoom, id: hostelToFind.rooms.length + 1});
  hostelToFind.roomNumbers = hostelToFind.rooms.length;
  return hostelToFind;
}

const newRoom = {
  roomName: 'ma nouvelle chambre',
  size: 4,
};

const myHostelId = 1;