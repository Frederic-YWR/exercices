// exercice 11 : faire une fonction qui prend en paramètre la liste des hotels
// et qui renvoie dans un tableau toutes les chambres qui ont plus de 3 places
// et qui sont dans un hotel avec piscine
// puis qui ajoute à chaque chambre une propriété "hotelName"
// qui contient le nom de l'hotel à laquelle il appartient


function exercice11 () {
    
    let hostels =  getHostel().filter(pools).map(hostel11)
                   .reduce(function(a, b) {return a.concat(b);})
                   .filter(by_size);

    
    return hostels;
    
}

function by_size(value) {
    
    return value.size > 3
    
}
function hostel11 (value,index) {
    
    let hostel = value.rooms.map(room,value.name);
    return hostel;
    
}

function room (value,index) {

    value.hotelName = this.toString();
    return value;
}
function pools(value,index) {
    
    return value.pool == true;
}



///Solution

function solution_ex11() {
    
    function getRoomsWithMoreThan3InHostelWithPool(value) {
  return value
      .filter((hotel) => hotel.pool)
      .reduce((acc, hotel) => {
        hotel.rooms = hotel.rooms
            .filter((room) => room.size > 3)
            .map((room) => {
              room.hotelName = hotel.name;
              return room;
            });
        return acc.concat(hotel.rooms);
      }, []);
        
        
}
    return getRoomsWithMoreThan3InHostelWithPool(hostels);
}

