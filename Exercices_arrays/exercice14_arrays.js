// exercice 14 : faire une fonction qui prend en paramètre la liste des hotel et
// un id d'hotel et un id de chambre et qui supprime cette chambre de l'hotel concerné


function exercice14 (hostel_list,hostel_id,room_id) {
    
    
    let indexed_hotels = hostel_list.map(fill_by_index);
    let targeted_hotel = indexed_hotels.indexOf(hostel_id);
    let indexed_rooms = hostel_list[targeted_hotel].rooms.map(fill_by_index);
    let targeted_room  = indexed_rooms.indexOf(room_id);
    hostel_list[targeted_hotel].rooms.splice(targeted_room,1);
    
    
    return hostel_list ;
}

function  fill_by_index (value) {
   
    return value.id;
}

//Solution

function findHostelIndex(hostels, hostelId) {
  return hostels.findIndex((hotel) => hotel.id === hostelId);
}

function solution_ex14(hostels, hostelId, roomId) {
  const hotelIndex = findHostelIndex(hostels, hostelId);
  if (hotelIndex) {
    hostels[hotelIndex].rooms = hostels[hotelIndex].rooms.filter(
        (room) => room.id !== roomId);
  }
  return hostels;
}

