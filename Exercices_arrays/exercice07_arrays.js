// exercice 7 : faire une fonction qui prend en paramètre un id d'hotel et qui retourne son nom


function exercice7 (hostel_id) {
    
    let hostels = getHostel();
    let indexes = hostels.map(fill_by_index);
    let target  = indexes.indexOf(hostel_id);
    if(hostels[target] === undefined) {
        return "désolé id incorrect"
    } else  {
        return hostels[target].name;
    }
}
function  fill_by_index (value) {
   
    return value.id;
}

//Solution

function findHostelById(hostels, hotelId) {
  const hostelToFind = hostels && hotelId
      ? hostels.find((hotel) => hotel.id === hotelId)
      : null;
  return hostelToFind ? hostelToFind : null;
}

function solution_ex7(hotelId) {
  const hostelToFind = findHostelById(getHostel(), hotelId);
  return hostelToFind && hostelToFind.name ? hostelToFind.name : '';
}