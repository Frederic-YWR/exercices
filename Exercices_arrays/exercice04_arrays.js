// exercice 4 : enlever de la liste des hotels toutes les chambres qui ont plus
// de 3 places et changer la valeur de roomNumbers pour qu'elle reflete
// le nouveau nombre de chambres


function exercice4 () {
    
    
    
  return getHostel().map(hostel);;
}

function hostel (user,index) {
  
  user.roomNumbers =user.rooms.filter(filtre).length;
  user.rooms = user.rooms.filter(filtre);
    
    return user
    
}

function filtre (user) {
    
    return user.size > 3;
}

//Solution


function solution_ex4 () {
    
  const roomsWithMoreThan3 = getHostel().map((hotel) => {
  hotel.rooms = hotel.rooms.filter((room) => room.size > 3);
  hotel.roomNumbers = hotel.rooms.length;
  return hotel;
  });
    return roomsWithMoreThan3;

}

