// exercice 13 : faire une fonction qui prend en paramètre la liste des hotel et
// un id d'hotel et qui supprime cet hotel de la liste des hotels



function exercice_13 (hotel_name,hotel_id) {
    
 
    
    let result = hotel_name.filter(erase,hotel_id);
    
    return result
    
}

function erase(value,name) {
    
    let number = parseInt(this);
    return value.id != number;
}

//solution

function findHostelById(hostels, hotelId) {
  const hostelToFind = hostels && hotelId
      ? hostels.find((hotel) => hotel.id === hotelId)
      : null;
  return hostelToFind ? hostelToFind : null;
}

function solution_ex13(hostels, hostelId) {
  const hostelToFind = findHostelById(hostels, hostelId);
  if (hostelToFind) {
    return hostels.filter((hotel) => hotel.id !== hostelId);
  }
  return hostels;
}