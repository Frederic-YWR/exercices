// exercice 17 : faire une fonction qui prend en paramètre la liste des hotels,
// une id d'hotel, un id de chambre et un nouveau nom de chambre qui va changer le nom
// de la chambre indiquée par le nouveau nom


function exercice17(hostel_list,hostel_id,room_id,roomname) {
    
    let indexed_hotels = hostel_list.map(fill_by_index);
    let targeted_hotel = indexed_hotels.indexOf(hostel_id);
    let indexes = hostel_list[targeted_hotel].rooms.map(fill_by_index);
    let target  = indexes.indexOf(room_id);
    
   hostel_list[targeted_hotel].rooms[target].roomName = roomname;
    
    return hostel_list;
}
function  fill_by_index (value) {
   
    return value.id;
}

//Solution

function findHostelById(hostels, hotelId) {
  const hostelToFind = hostels && hotelId
      ? hostels.find((hotel) => hotel.id === hotelId)
      : null;
  return hostelToFind ? hostelToFind : null;
}
function findRoomByHotelIdAndRoomId(hostels, hostelId, roomId) {
  const hostelToFind = findHostelById(hostels, hostelId);
  const roomToFind = hostelToFind && hostelToFind.rooms && roomId
      ? hostelToFind.rooms.find((room) => room.id === roomId)
      : null;
  return roomToFind ? roomToFind : null;
}

function changeRoomName(hotelId, roomId, newNamme) {

  const hostelsList = getHostel();
  const roomToChange = findRoomByHotelIdAndRoomId(hostelsList, hotelId,
      roomId);
  roomToChange.roomName = newNamme;
  return hostelsList;
}