// exercice 10 : faire une fonction qui prend en paramètre la liste des hotels
// et qui renvoie un tableau contenant les id des hotels
// qui ont au moins une chambre avec plus ou exactement 5 places


function exercice10() {
    
     return getHostel().map(hostel)
            .map(set_hostels)
            .filter(definied);
    
}
function definied (value) {
    
    return value != undefined
}
function hostel(value) {
    
    return value.rooms.some(room);
}
function room(value) {
    
      return value.size >= 5;
}

function set_hostels(value,index) {
    
    
     if (value === true ) {
         
         return getHostel()[index].id
         
     } 
}


function solution_ex10 (arg) {

    function hotelHasRoomWithMoreThan5(hotel) {
  return hotel.rooms.some((room) => room.size >= 5);
}

function getIdOfAllHostelWithFive(hostels) {
  return hostels.reduce((acc, hotel) => {
    if (hotelHasRoomWithMoreThan5(hotel)) {
      return acc.concat(hotel.id);
    } else {
      return acc;
    }
  }, []);
}
    return getIdOfAllHostelWithFive(arg)
}
