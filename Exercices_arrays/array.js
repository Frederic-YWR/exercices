// on fait une fonction qui copie la liste des hotels pour pouvoir la réutiliser
// autant qu'on le veut sans la modifier
import {getHostel} from './hotels.data';

// exercice 0 : mettre une majuscule à toutes les RoomName

console.log('[ARRAY]Exo0 : ', capiAll());
function capiAll() {
  return getHostel();
}


// exercice 1 : trier les hotels par nombre
// de chambres (plus grand en 1er) et créer un tableau
// contenant seulement
// le nom des hotels dans leur ordre de tri

console.log('[ARRAY]Exo1 : ', '');

// exercice 2 : faire un tableau avec toutes les chambres de tous les hotels,
// et ne garder que les chambres qui
// ont plus que 3 places ou exactement 3 places et
// les classer par ordre alphabétique selon le non de la chambre

console.log('[ARRAY]Exo2 : ', '');

// exercice 3 : faire un tableau avec toutes les chambres des hotels
// et ne garder que les chambres qui ont plus de 3 places et dont le
// nom de chambre a une taille supérieure à 15 charactères

console.log('[ARRAY]Exo3 : ', '');

// exercice 4 : enlever de la liste des hotels toutes les chambres qui ont plus
// de 3 places et changer la valeur de roomNumbers pour qu'elle reflete
// le nouveau nombre de chambres

console.log('[ARRAY]Exo4 : ', '');

// exercice 5  : extraire du tableau hostels l'hotel qui a le nom 'hotel ocean' en le supprimant du tableau,
// et le mettre dans une nouvelle variable
// puis effacer toutes ses chambres et mettre à jour sa valeur room number, puis pusher l'hotel modifié dans hostels
// puis faire un sort par nom d'hotel
// puis donner le nouvel index de l'hotel océan (faire 2 méthodes : avec indexOf et avec un foreach)

console.log('[ARRAY]Exo5 : ', '');

// exercice 6 : créer un objet dont les clés sont le nom des hotels et dont la valeur est un booléen qui indique
// si l'hotel a une chambre qui s'appelle 'suite marseillaise'

console.log('[ARRAY]Exo6 : ', '');

// exercice 7 : faire une fonction qui prend en paramètre un id d'hotel et qui retourne son nom

console.log('[ARRAY]Exo7 : ', '');

// exercice 8 : faire une fonction qui prend en paramètre un id d'hotel et un id de chambre et qui retourne son nom

console.log('[ARRAY]Exo8 : ', '');

// exercice 9: faire une fonction qui prend en paramètre la liste des hotels
// et qui vérifie que toutes les chambres des hotels ont bien
// une majuscule a leur nom et qui renvoie un boolean donnant le résultat.
// Puis faire en sorte que la liste des hotels valide bien cette fonction.

console.log('[ARRAY]Exo9 : ', '');
console.log('[ARRAY]Exo9 avec maj: ', '');

// exercice 10 : faire une fonction qui prend en paramètre la liste des hotels
// et qui renvoie un tableau contenant les id des hotels
// qui ont au moins une chambre avec plus ou exactement 5 places

console.log('[ARRAY]Exo10 : ', '');

// exercice 11 : faire une fonction qui prend en paramètre la liste des hotels
// et qui renvoie dans un tableau toutes les chambres qui ont plus de 3 places
// et qui sont dans un hotel avec piscine
// puis qui ajoute à chaque chambre une propriété "hotelName"
// qui contient le nom de l'hotel à laquelle il appartient

console.log('[ARRAY]Exo11 : ', '');

// exercice 12 : faire une fonction qui prend en paramètre la liste des hotel,
// un id d'hotel et une taille et qui renvoie le nom et l'id de toutes
// les chambres de cet hotel qui font exactement la taille indiquée

console.log('[ARRAY]Exo12 : ', '');

// exercice 13 : faire une fonction qui prend en paramètre la liste des hotel et
// un id d'hotel et qui supprime cet hotel de la liste des hotels

console.log('[ARRAY]Exo13 : ', '');

// exercice 14 : faire une fonction qui prend en paramètre la liste des hotel et
// un id d'hotel et un id de chambre et qui supprime cet chambre de l'hotel concerné

console.log('[ARRAY]Exo14 : ', '');

// exercice 15 : faire une fonction qui prend en paramètre la liste des hotels,
// une id d'hotel et un boolean et qui va changer la valeur de l'attribut "pool"
// dans l'hotel concerné avec le boolean donné

console.log('[ARRAY]Exo15 : ', '');

// exercice 16 : faire une fonction qui prend en paramètre la liste des hotels,
// une id d'hotel et une nouvelle chambre (avec son nom et sa taille) qui
// ajoute la nouvelle chambre dans l'hote concerné et lui donne un id qui suit le
// dernier id de l'hotel// 

console.log('[ARRAY]Exo16 : ', '');

// exercice 17 : faire une fonction qui prend en paramètre la liste des hotels,
// une id d'hotel, un id de chambre et un nouveau nom de chambre qui va changer le nom
// de la chambre indiquée par le nouveau nom

console.log('[ARRAY]Exo17 : ', '');
