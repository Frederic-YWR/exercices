// exercice 8 : faire une fonction qui prend en paramètre un id d'hotel et un id de chambre et qui retourne son nom

function exercice8 (hostel,room_id) {
    
    let hostels_index = getHostel().map(fill_by_index);
    let hostel_target = hostels_index.indexOf(hostel);
    let room_indexes = getHostel()[hostel_target].rooms.map(fill_by_index);
    let room_target  = room_indexes.indexOf(room_id);
    
    return getHostel()[hostel_target].rooms[room_target].roomName;
}

function  fill_by_index (value) {
   
    return value.id;
}

//Solution

 function findRoomByHotelIdAndRoomId(hostels, hostelId, roomId) {
  const hostelToFind = findHostelById(hostels, hostelId);
  const roomToFind = hostelToFind && hostelToFind.rooms && roomId
      ? hostelToFind.rooms.find((room) => room.id === roomId)
      : null;
  return roomToFind ? roomToFind : null;
}

function findHostelById(hostels, hotelId) {
  const hostelToFind = hostels && hotelId
      ? hostels.find((hotel) => hotel.id === hotelId)
      : null;
  return hostelToFind ? hostelToFind : null;
}
function solution_ex9(hotelId, roomId) {
  const roomToFind = findRoomByHotelIdAndRoomId(getHostel(), hotelId, roomId);
  return roomToFind && roomToFind.roomName ? roomToFind.roomName : '';
}

