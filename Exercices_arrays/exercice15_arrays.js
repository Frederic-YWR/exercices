// exercice 15 : faire une fonction qui prend en paramètre la liste des hotels,
// une id d'hotel et un boolean et qui va changer la valeur de l'attribut "pool"
// dans l'hotel concerné avec le boolean donné


function exercice15(hostels, hostelId, poolValue) {
    
    let indexes = hostels.map(fill_by_index);
    let target  = indexes.indexOf(hostelId);
    
    hostels[target].pool = poolValue;
    
    return hostels
}


function  fill_by_index (value) {
   
    return value.id;
}


//Solution 


function findHostelIndex(hostels, hostelId) {
  return hostels.findIndex((hotel) => hotel.id === hostelId);
}

function solution_ex15(hostels, hostelId, poolValue) {
  const hotelIndex = findHostelIndex(hostels, hostelId);
  if (hotelIndex) {
    hostels[hotelIndex].pool = poolValue;
  }
  return hostels;
}