// exercice 15 : faire une fonction qui prend en paramètre la liste des hotels,
// un id d'hotel et un boolean et qui va changer la valeur de l'attribut "pool"
// dans l'hotel concerné avec le boolean donné




function exercice15_object(hostels, hostelId, poolValue) {
    
    let roomKeys = Object.keys(hostels);
    let roomValues = Object.values(hostels);
    let hotel_index = Object.values(hostels).map((value)=> value.id);
    let hotel_target = hotel_index.indexOf(hostelId);
    
    hostels[roomKeys[hotel_target]]["pool"] = poolValue
    
    return hostels
}

