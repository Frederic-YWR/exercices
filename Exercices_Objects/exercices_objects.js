function exercice1_objects() {
    
    let lessons_keys = Object.keys(lessons);
    let lessons_values = Object.values(lessons);
    
    let keys_numbers = lessons_keys.map((value)=> {
         
        let len = value.indexOf("n")+1;
        
    
        
         return  parseInt(value.substr(len));
    });
    
    
    let ordered_keynumbers = keys_numbers.sort((a,b) => a - b);
    let obj = {};
    
    ordered_keynumbers.forEach((value,index) => obj["lesson"+value] = lessons["lesson"+value])
    
    return obj
}


function exercice2_objects () {
    
    let lessons_object =  exercice1_objects();
    
    let lessons_keys = Object.keys(lessons_object);
    
    let lessons_values = Object.values(lessons_object);
    
    let first_false = lessons_values.indexOf(false);
    
    let len = lessons_keys[first_false].indexOf("n")+1;
        
    
    let obj = {};
    
    obj["key"] = parseInt(lessons_keys[first_false].substr(len));
    
    obj["value"]= lessons_values[first_false]
    
    return obj
}