// exercice 5  : extraire du tableau hostels l'hotel qui a le nom 'hotel ocean' en le supprimant du tableau,
// et le mettre dans une nouvelle variable
// puis effacer toutes ses chambres et mettre à jour sa valeur room number, puis pusher l'hotel modifié dans hostels
// puis faire un sort par nom d'hotel
// puis donner le nouvel index de l'hotel océan (faire 2 méthodes : avec indexOf et avec un foreach)


function exercice5_object () {
    
    let hotelValues = Object.values(hostels);
    let hotel_by_index = Object.values(hostels).map((value)=>value.name)
    let hotelKeys   = Object.keys(hostels);
    
    hotelValues.forEach((value,index)=>{
        if(value["name"] == 'hotel ocean') {
        hotelValues[index]["rooms"] = {};
        hotelValues[index]["roomNumbers"] = 0;
        }
    });
    
    let newHotelValues = Object.values(hotelValues);
    let assocArray = [];
    
     let  obj = {};
    
    newHotelValues.forEach((value,index) => 
                           {
        assocArray[hotelKeys[index]] = value;
    })
    
   let sortedHotels = Object.values(assocArray).sort((a, b) => b.name < a.name ? 1 : -1);
   
   sortedHotels.forEach((value,index)=>{ 
                                        
                                       obj[hotelKeys[hotel_by_index.indexOf(value.name)]] = value;
                                        
                                 });
    
      
    
    return obj;
}
