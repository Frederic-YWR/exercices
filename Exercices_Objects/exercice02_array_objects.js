// exercice 2 : faire un tableau avec toutes les chambres de tous les hotels,
// et ne garder que les chambres qui
// ont plus que 3 places ou exactement 3 places et
// les classer par ordre alphabétique selon le non de la chambre




function  exercice2_array()  {
    
    let ranking= Object.values(hostels).map((value) => Object.values(value.rooms)
                                       .filter((value2)=>  value2.size >= 3))
    .reduce((a,b) =>  a.concat(b))
    .sort((a,b) => b.roomName < a.roomName ? 1 : -1 );
                                    
     return ranking
    
}
function  exercice2_object()  {

    let ranking = Object.values(hostels).map((value) => Object.values(value.rooms)
                                       .filter((value2)=>  value2.size >= 3))
    .reduce((a,b) =>  a.concat(b))
    .sort((a,b) => b.roomName < a.roomName ? 1 : -1 );
    
     
    let obj = {};
    
    ranking.forEach((value,index)=> obj[index + 1] = value);
    
    return obj;
    
                                    
     return obj
    
}