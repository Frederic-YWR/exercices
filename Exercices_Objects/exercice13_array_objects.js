// exercice 13 : faire une fonction qui prend en paramètre la liste des hotel et
// un id d'hotel et qui supprime cet hotel de la liste des hotels



function exercice13_object (hotel_list,hotel_id) {
    
    let hostelsValues = Object.values(hotel_list);
    let hotelNames = Object.values(hotel_list).map((value) => value.id);
    let hostel_keys  = Object.keys(hotel_list);
    let target  = hotelNames.indexOf(hotel_id);
    var obj = {};
    let newHotelList = hostel_keys.filter((value,index) => index != target);

    newHotelList.forEach((value,index)=> obj[value] = hostels[value]);
   
  
    return obj
}