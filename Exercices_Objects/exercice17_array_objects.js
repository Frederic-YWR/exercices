// exercice 17 : faire une fonction qui prend en paramètre la liste des hotels,
// une id d'hotel, un id de chambre et un nouveau nom de chambre qui va changer le nom
// de la chambre indiquée par le nouveau nom

function  fill_by_index (value) {
   
    return value.id;
}  


function exercice17_object (hostels,hostelId,roomId,newName) {
    
    let hostels_list    = Object.values(hostels);
    let hostels_list_keys = Object.keys(hostels);
    let indexed_hotels = Object.values(hostels).map(fill_by_index);
    let targeted_hotel = indexed_hotels.indexOf(hostelId);
    let arrayOfRooms = hostels_list[targeted_hotel].rooms;
    
    let zones = Object.values(arrayOfRooms);
    let zones_id = Object.values(arrayOfRooms).map(fill_by_index);
    let targeted_room = zones_id.indexOf(roomId);
    let zones_keys = Object.keys(arrayOfRooms);
    
    hostels[hostels_list_keys[targeted_hotel]]["rooms"][zones_keys[targeted_room]]["roomName"] = newName;
    
    return hostels
}