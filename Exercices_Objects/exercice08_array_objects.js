// exercice 8 : faire une fonction qui prend en paramètre un id d'hotel et un id de chambre et qui retourne son nom


function  fill_by_index (value) {
   
    return value.id;
}

function exercice8_object(hostel,room_id) {
    
    let hotels_rooms = Object.values(hostels).map((value)=> Object.values(value.rooms));
    let hostels_index = Object.values(hostels).map(fill_by_index);
    let hostel_target = hostels_index.indexOf(hostel);
    let room_indexes = hotels_rooms[hostel_target].map(fill_by_index);
    let room_target  = room_indexes.indexOf(room_id);
    
    return  hotels_rooms[hostel_target][room_target].roomName
    
}
