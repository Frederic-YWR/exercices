// exercice 16 : faire une fonction qui prend en paramètre la liste des hotels,
// une id d'hotel et une nouvelle chambre (avec son nom et sa taille) qui
// ajoute la nouvelle chambre dans l'hote concerné et lui donne un id qui suit le
// dernier id de l'hotel



function  fill_by_index (value) {
   
    return value.id;
}



function compare(num1, num2) {
    if(num1 < num2) return num2;
}


function exercice16_object (hostels,hostelId,newRoom,newSize) {
    
    let hostels_list    = Object.values(hostels);
    let hostels_list_keys = Object.keys(hostels);
    let indexed_hotels = Object.values(hostels).map(fill_by_index);
    let targeted_hotel = indexed_hotels.indexOf(hostelId);
    
    let arrayOfRooms = hostels_list[targeted_hotel].rooms;
    
    let zones = Object.values(arrayOfRooms);
    let zones_id = Object.values(arrayOfRooms).map(fill_by_index);
    let zones_keys = Object.keys(arrayOfRooms);
    let new_index = zones_id.reduce(compare)+1;
    
    hostels[hostels_list_keys[targeted_hotel]]["rooms"]["room"+new_index] = {roomName:newRoom,size:newSize ,id:new_index};
    
     hostels[hostels_list_keys[targeted_hotel]]["rooms"]["roomNumbers"] += 1;
    
    
    return hostels
}

