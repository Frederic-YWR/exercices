// exercice 14 : faire une fonction qui prend en paramètre la liste des hotel et
// un id d'hotel et un id de chambre et qui supprime cette chambre de l'hotel concerné

 

function exercice14_object(hostel_id,room_id) {
    
    let hostels_list  = Object.values(hostels);
    let hostels_keys  = Object.keys(hostels);
    let indexed_hotels = Object.values(hostels_list ).map(fill_by_index); 
    let targeted_hotel = indexed_hotels.indexOf(hostel_id);
    let rooms_values = Object.values(hostels)[targeted_hotel]["rooms"];
    
    let room_value = Object.values(rooms_values);
    let indexed_rooms = Object.values(rooms_values).map(fill_by_index);
    let targeted_room  = indexed_rooms.indexOf(room_id);
    let room_key = Object.keys(rooms_values);
    
    let obj = {};
    
    room_value.forEach((value,index) =>{if(index != targeted_room) obj[room_key[index]] = value });

    hostels[hostels_keys[targeted_hotel ]]["rooms"] = obj;
    hostels[hostels_keys[targeted_hotel ]]["roomNumbers"] -= 1;
   return  hostels  ;
} 
