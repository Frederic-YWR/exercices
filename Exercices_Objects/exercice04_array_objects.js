// exercice 4 : enlever de la liste des hotels toutes les chambres qui ont plus
// de 3 places et changer la valeur de roomNumbers pour qu'elle reflete
// le nouveau nombre de chambres




function exercice4_object() {

        
        let hostelKeys = Object.values(hostels);
    
        let hostelIndex = Object.keys(hostels)
        
        let rooms_values = Object.values(hostels).map((hotel)=> Object.values(hotel.rooms).map((room)=> room));
    
        let rooms_keys = Object.values(hostels).map((hotel)=> Object.keys(hotel.rooms).map((room)=> room));
        
        let array = [];
    
        rooms_values.forEach((value,index) =>{
                                               let arr = [];
                                               value.forEach((value2,index2)=> {
                                               if(value2["size"] > 3) {
                                                   arr[rooms_keys[index][index2]] = value2}});
                                                array.push(arr)});
    
         array.forEach((value,index)=> {hostels[hostelIndex[index]]["rooms"] = value;
                                      hostels[hostelIndex[index]]["roomNumbers"] = Object.values(value).length})
        
         return hostels;
    
}

