// exercice 3 : faire un tableau avec toutes les chambres des hotels
// et ne garder que les chambres qui ont plus de 3 places et dont le
// nom de chambre a une taille supérieure à 15 charactères

function exercice3_array() {

        
        let hostelKeys = Object.keys(hostels);
    
        let hotel = Object.values(hostels).map((value) => Object.values(value.rooms)
        .filter((value2)=> value2.roomName.length > 15 && value2.size > 3 )
         ).reduce((a,b) => a.concat(b),[])
    
        return hotel;
    
}
function exercice3_obj() {

        
        let hostelKeys = Object.keys(hostels);
    
        let hotel = Object.values(hostels).map((value) => Object.values(value.rooms)
        .filter((value2)=> value2.roomName.length > 15 && value2.size > 3 )
         ).reduce((a,b) => a.concat(b),[]);
        
        let obj = { }
        
        hotel.forEach( (value,index) => {
           obj[index] = value;
        })
        
        
        
    
        return obj;
    
}