// exercice 6 : créer un objet dont les clés sont le nom des hotels et dont la valeur est un booléen qui indique
// si l'hotel a une chambre qui s'appelle 'suite marseillaise'
function exercice6_object () {
    
    const tab = Object.values(hostels).map((value) => {
    let obj = {
                key:value.name,
                value:Object.values(value.rooms).some((value) => value.roomName == "suite marseillaise")
    }
       return obj 
    });
  
return tab;
}

