const user = {
    
    firstname : 'YEBOUA',
    lastname : 'Anouma',
    age : 32,
    billionaire : 'I hope one day',
    eyes : 'brown',
    address : 'None',
};




//Object destructuration

const {firstname,lastname} = user;

//Object Attribute Access

const attrs = ['firstname','lastname'];

attrs.forEach( attr => console.log(user[attr]));


