
// exercice 1 : trier les hotels par nombre
// de chambres (plus grand en 1er) et créer un objet
// contenant seulement
// le nom des hotels dans leur ordre de tri



function exercice1_object() {
    
    
        let hotelList = Object.values(hostels).sort((a, b) => b.roomNumbers - a.roomNumbers)
                    .map((hostel) => hostel.name);
        
        let obj = {};
    
        hotelList.forEach((value,index)=> obj[index + 1] = value);
    
        return obj;
    
}

function exercice1_array() {
    
    
        let hotelList = Object.values(hostels).sort((a, b) => b.roomNumbers - a.roomNumbers)
                    .map((hostel) => hostel.name);
        
    
        return hotelList;
    
}



