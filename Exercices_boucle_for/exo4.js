// exercice 4 : générer un tableau contenant des nombres pairs consécutifs,
// le premier nombre du tableau doit être 4,
// on doit arreter de remplir le tableau quand il y a 20 nombres pairs dans le tableau

function exercice4() {
    
    
    let array = [];
    let number = 4;
    
    for(let i=0; i < 20 ; ++i) {
        
        array.push(number);
        number += 2;
        
    }
return array
}