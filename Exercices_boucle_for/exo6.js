// exercice 6 : Écrire un programme qui affiche les nombres de 1 à 199 avec
// un console log.
// Mais pour les multiples de 3, afficher “Fizz” au lieu du nombre et pour les multiples de 5 afficher “Buzz”.
// Pour les nombres multiples de 3 et 5, afficher “FizzBuzz”.
// INFO Importante : cet exercice est le plus utilisé au monde dans les tests

function exercice6() {
    
    for(let i = 1 ;i < 200; i++) {
        
             if ((i%3 == 0) && (i%5==0)) { 
                        console.log("FizzBuzz");
                }  
             else if (i%5 == 0) {
                        console.log("Buzz");
               } 
             else if (i%3 == 0) {
                       console.log("Fizz");
               } 
             else {
                       console.log(i);
             }
  
          }
    return
}
