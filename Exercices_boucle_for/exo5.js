// exercice 5 : générer un array avec 100 objets avec la forme ci dessous, dont les données sont toutes aléatoires

// const person = {firstName: '', lastName: '', age: 0};
// console.log(person);

// bonus : calculer la moyenne des âges de personnes en utilisant un reduce

 function exercice5() {
     
     let result = [];
     
     const firstName = ['joe', 'jane', 'seb','alina','fabien','merwan','anna','annah','Fathma',
                        'Mohamed','Walid','Josiane'];
     const lastName = [ 'Dupont','Dupond','Durand','Kassir','Dalachra','Hussein','Wartani','Thoumsi','Angello',];
     
     function getName(array) {
         
         return array[Math.floor(0 + Math.random() * (array.length - 0) )]
     }
     
     function getYear() {
         
         return Math.floor(0 + Math.random() * (40 - 0) + 1)
     }
     
     for(let i=0;i < 100;i++) {
         
         let obj = {firstName:getName(firstName),lastName:getName(lastName),age:getYear()};
         
         result.push(obj);
     }
     
     let yearsArray = result.map((value)=>value.age);
     
     let average = yearsArray.reduce((a,b)=> a+b)/yearsArray.length;
     
     return average
 }
