// exercice 2 : créer une fonction qui prend en paramètre 2 nombres entiers et qui
// affiche tous les entiers entre le premier et le second

function exercice2(number1, number2 ) {
    
     if( typeof number1 === "number" && typeof number2 === "number"){
         
        if( number1 < number2) {
            
             for(let i = number1; i < number2+1; i++) {
                 
                  console.log(i);
              }
        } else {
              for(i=number2;i < number1+1;i++) {
                  console.log(i);
              }
          }
       } else {
    
           return "Veuillez entrer un nombre entier"
        } 
}