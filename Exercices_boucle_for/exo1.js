// exercice 1 : créer une fonction qui prend en paramètre un nombre entier et qui
// affiche tous les entiers entre 0 et lui même

function exercice1(number) {
    var i = 0;
    if (typeof number === "number") {
    
        for (let i = 0; i < number + 1; i++) {
            
            console.log(i);
        }
    } else {
    
         return "Veuillez entrer un nombre entier"; 
     }        
}
