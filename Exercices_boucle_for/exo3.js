// exercice 3 : créer une fonction qui génére un tirage de loto avec
// 7 nombres compris entre 1 et 45
     
   function exercice3() {
       
   let arr = [];
       
   let tirage;
       
   let last;
       
   for(let i=0;i<7;i++){
        
        tirage = Math.floor(1 + Math.random() * (45-1)+1);
        
        if ( arr.length > 1 ) {
            last = arr[arr.length - 1];
            while ( tirage == last ){
                tirage = Math.floor(1 + Math.random() * (45-1)+1); 
            }
            
         arr.push(tirage); 
         } else {
              tirage = Math.floor(1 + Math.random() * (45-1)+1);
              arr.push(tirage); 
         }
     }
       
       
    return  arr;
       
   }